package api;

import java.util.ArrayList;

import jdk.nashorn.internal.parser.JSONParser;
import model.data_structures.LinkedListInterfaz;
import model.data_structures.LinkedLists;
import model.vo.Taxi;
import model.vo.Service;

/**
 * Basic API for testing the functionality of the TaxiTrip manager
 */
public interface ITaxiTripsManager {

	/**
	 * Method to load taxi services
	 * @param servicesFile - path to the file 
	 */
	void loadServices(String serviceFile);

    /**
	 * Method to return all the taxis for a given comapany
	 * @param company - Taxi company
	 * @return List of taxis
	 */
	public LinkedLists<Taxi> getTaxisOfCompany(String company);
	
	/**
	 * Method to return all the services finished in a communityArea
	 * @param communityArea 
	 * @return List of services
	 */
	public LinkedListInterfaz<Service> getTaxiServicesToCommunityArea(int communityArea);


	
}
