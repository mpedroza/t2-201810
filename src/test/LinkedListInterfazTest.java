package test;

import java.util.LinkedList;

import junit.framework.TestCase;
import model.data_structures.LinkedListInterfaz;
import model.data_structures.LinkedLists;
import sun.security.util.PendingException;

public class LinkedListInterfazTest extends TestCase{

	private LinkedLists<String> lista;
	
	private void setupEscenario1()
	{
			lista = new LinkedLists<String> ();
			lista.add("Dato 1");
			lista.add("Dato 2");
			lista.add("Dato 3");
	}
	
	public void testAddGet () 
	{
		setupEscenario1();
		
		
		
		//System.out.println("Next: "+lista.next());
		
		assertEquals("El valor del primer dato de la lista es incorrecto", "Dato 1", lista.get("Dato 1"));
		assertEquals("El valor del segundo dato de la lista es incorrecto", "Dato 2", lista.get("Dato 2"));
		assertEquals("El valor del tercer dato de la lista es incorrecto", "Dato 3", lista.get("Dato 3"));
		
		
	}
	
	public void testGetSize () 
	{
		setupEscenario1();
		
		assertEquals("El tamano de la lista es incorrecto", 3, lista.getSize());
		
	}
	
	public void testGetCurrent()
	{
		setupEscenario1();
		
		assertEquals("El dato actual (primer dato) es incorrecto", "Dato 1", lista.getCurrent());
		
		lista.next();
		
		assertEquals("El dato actual (segundo dato) es incorrecto", "Dato 2", lista.getCurrent());
		
		lista.next();
		
		assertEquals("El dato actual (segundo dato) es incorrecto", "Dato 3", lista.getCurrent());
		
		
	}
	
	public void testListing() {
		
		setupEscenario1();
		
		lista.next();
		lista.next();
		lista.listing();
		
		assertEquals("El dato actual (Listing) es incorrecto", "Dato 1", lista.getCurrent());
		
	}
	
	public void testDelete() {
		
		setupEscenario1();
		
//		lista.delete("Dato 1");
		lista.delete("Dato 2");
		lista.delete("Dato 3");
		
		System.out.println("Test");
		System.out.println("1: "+lista.get("Dato 1"));
		System.out.println("2: "+lista.get("Dato 2"));
		System.out.println("3: "+lista.get("Dato 3"));
		
		assertEquals("los datos no se eliminaron", 1, lista.getSize());
		
		
	}
	
	
	
}
