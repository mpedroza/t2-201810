package model.vo;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

/**
 * Representation of a Service object
 */
public class Service implements Comparable<Service> {

	JsonObject service = null;

	public Service (JsonObject pservice) {
		service = pservice;
	}

	/**
	 * @return id - Trip_id
	 */
	public String getTripId() {
		// TODO Auto-generated method stub
		
		return service.get("trip_id").getAsString();
	}	
	
	/**
	 * @return id - Taxi_id
	 */
	public String getTaxiId() {
		// TODO Auto-generated method stub
		return service.get("taxi_id").getAsString();
	}	
	
	/**
	 * @return time - Time of the trip in seconds.
	 */
	public int getTripSeconds() {
		// TODO Auto-generated method stub
		return service.get("trip_seconds").getAsInt();
	}

	/**
	 * @return miles - Distance of the trip in miles.
	 */
	public double getTripMiles() {
		// TODO Auto-generated method stub
		return service.get("trip_miles").getAsDouble();
	}
	
	/**
	 * @return total - Total cost of the trip
	 */
	public double getTripTotal() {
		// TODO Auto-generated method stub
		return service.get("trip_total").getAsDouble();
	}

	@Override
	public int compareTo(Service o) {
		// TODO Auto-generated method stub
		return Integer.parseInt(this.getTaxiId()) - Integer.parseInt(o.getTaxiId());
	}
}
