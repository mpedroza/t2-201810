package model.logic;

import api.ITaxiTripsManager;
import controller.Controller;
import jdk.nashorn.internal.parser.JSONParser;
import model.vo.Taxi;
import model.vo.Service;
import model.data_structures.LinkedListInterfaz;
import model.data_structures.LinkedLists;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;

import com.google.gson.JsonArray;
import com.google.gson.JsonIOException;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonSyntaxException;


public class TaxiTripsManager implements ITaxiTripsManager {

	// TODO
	// Definition of data model 

	private JsonArray lista;
	String serviceFile = "./data/taxi-trips-wrvz-psew-subset-small.json";


	public void loadServices (String serviceFile) {

		LinkedLists<String> listaTaxis = new LinkedLists<String> ();

		// TODO Auto-generated method stub
		JsonParser parser = new JsonParser();

		try {
			String taxiTripsDatos = serviceFile;

			/* Cargar todos los JsonObject (servicio) definidos en un JsonArray en el archivo */
			lista= (JsonArray) parser.parse(new FileReader(taxiTripsDatos));
		}


		catch (JsonIOException e1 ) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		catch (JsonSyntaxException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}
		catch (FileNotFoundException e3) {
			// TODO Auto-generated catch block
			e3.printStackTrace();
			System.out.println("Inside loadServices with " + serviceFile);

		}

		
	}

	@Override
	public LinkedLists<Taxi> getTaxisOfCompany(String company) {
		// TODO Auto-generated method stub

		LinkedLists<Taxi> listaTaxis = new LinkedLists<Taxi> ();
		LinkedLists<Taxi> idTaxis = new LinkedLists<Taxi> ();
		Controller.loadServices( );
		String Taxi = null;
		String empresa = null;
		String id = null;
		Taxi taxi;
		Taxi taxiaux = null;
			

		for (int i = 0; lista != null && i < lista.size(); i++)
		{
			JsonObject obj= (JsonObject) lista.get(i);
					
			taxi = new Taxi(obj);	
			
			if(taxi.getCompany()!=null) {
			empresa = taxi.getCompany();
			
			
			if (empresa.equals(company) && taxi.compareTo(listaTaxis.get(taxi)) != 0)
			{				
				Taxi = "Empresa: "+empresa+", Id del Taxi: "+taxi.getTaxiId();
				listaTaxis.add(taxi);
				
				System.out.println(Taxi);
			}
			
			}
			

		}

		System.out.println("\n\nEmpresa a buscar: " + company);

		return listaTaxis;
	}

	@Override
	public LinkedListInterfaz<Service> getTaxiServicesToCommunityArea(int communityArea) {
		// TODO Auto-generated method stub


		LinkedLists<Service> listaTaxis = new LinkedLists<Service> ();
		Controller.loadServices( );
		int area = 0;
		Service servicio;
		String serv;

		for (int i = 0; lista != null && i < lista.size(); i++)
		{

			JsonObject obj= (JsonObject) lista.get(i);
			
			servicio = new Service(obj);

			if(obj.get("dropoff_community_area")!=null) 
			{
				area = obj.get("dropoff_community_area").getAsInt();
			}


			if(obj.get("dropoff_community_area") != null)
			{
				if (area == communityArea )
				{  

					serv = "Id del Taxi: "+servicio.getTaxiId()+", "+"Id del servicio: "+servicio.getTripId()+", "+"Millas del servicio: "+servicio.getTripMiles()+", "+"Segundos de duracion del servicio: "+servicio.getTripSeconds()+", "+"Total del servicio: "+servicio.getTripTotal();
					listaTaxis.add(servicio);

					System.out.println(serv);

				}	
			}

		}

		System.out.println("\n\nUbicacion a buscar: " + communityArea);

		return listaTaxis;


	}

}
